let chai = require('chai');
var expect    = require("chai").expect;
var mutation = require("../controllers/MutationController");
const sinon = require("sinon");
let chaiHttp = require('chai-http');

chai.use(chaiHttp);

let sinonChai = require('sinon-chai');
chai.use(sinonChai);

describe("Probando hasMutation", function() {
  describe("Test 1", function() {
    it("probando dna con mutation", function() {
      var res   = mutation.hasMutation(["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]);
      expect(res).to.equal(true);
    });
  });

  describe("Test 2", function() {
    it("probando dna sin mutacion", function() {
      var res   = mutation.hasMutation(["ATGCGA","CAGTGC","TTATTT","AGACGG","GCGTCA","TCACTG"]);
      expect(res).to.equal(false);
    });
  });

});


describe('Get stats: ',()=>{
   it('should obtener stats', (done) => {
     chai.request('http://localhost:3789')
     .get('/api/stats')
     .end( function(err,res){
       expect(res).to.have.status(200);
       done();
     });
   });
});



describe('Save dna info: ',()=>{
   it('should save dna', (done) => {
     chai.request('http://localhost:3789')
     .post('/api/mutation')
     .send({"dna":["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]})
     .end( function(err,res){
       expect(res).to.have.status(403);
       done();
     });
   });
});



describe("Chek dna", function() {
  let status, json, res;
  beforeEach(async () => {
    req = {
      body:{
        "dna":["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]
      }
    }
    status = sinon.stub();
    json = sinon.spy();
    res = { json, status };
    res.status.returns(res);
    res.json = sinon.stub().returns(res);
    res.send = sinon.spy();
  })
  it("deberia ser false", async () => {

    await  mutation.check(req, res)
    
    expect(status.calledOnce).to.be.false;
  });
});


describe("Chek stats", function() {
  let status, json, res;
  beforeEach(async () => {
    req = {
    }
    status = sinon.stub();
    json = sinon.spy();
    res = { json, status };
    res.status.returns(res);
    res.json = sinon.stub().returns(res);
    res.send = sinon.spy();
  })
  it("deberia ser false", async () => {

    await  mutation.stats(req, res)
    
    expect(status.calledOnce).to.be.false;
  });
});