'use strict'
//modulos

var Mutation = require('../models/mutations');
var moment = require('moment');

/**
* @method
* @desc Iniacia validacion de dna
* @version 1.0.0
* @param {object} req
* @param {object} res
*/
function check(req, res){
	if(req.body && req.body.dna && typeof req.body.dna == 'object'){

		var mutation = new Mutation();
		mutation.dna = req.body.dna;
		let tieneMutacion = hasMutation(req.body.dna)
		mutation.valido = !tieneMutacion;

		Mutation.findOne( {dna:mutation.dna}, (err, mutationFinded) => { 
			if(err){
				res.status(404).send({message: err});
			} else{ 
				if(!!mutationFinded) {
					res.status(403).send({
						tieneMutacion
					});
				} else {
					mutation.save((err, mutationStored) => {
						if(err){
							res.status(403).send({
								message: "No fue posible guardar",err
							});
						} else {
							if(!mutationStored) {
								res.status(403).send({message: "Guardado fallido"});
							} else {
								res.status(200).send({tieneMutacion});
							}
						}
					});
				}
				
			}
		});
		
	} else {
		res.status(403).send({
          	message: "DNA INCORRECTO"
        });
	}
}

/**
* @method
* @desc fila por fila verifica mutacion 
* @version 1.0.0
* @param {array} dna
* @returns {Boolean} hasMutation
*/
function hasMutation(dna){
	let totalMutacionesHorizontales = 0;
	let totalMutacionesVerticales = 0;
	let totalMutacionesOblicuasDer= 0;
	let totalMutacionesOblicuasIzq= 0;
	let tieneMutacion = false;
	dna.forEach(function(row) {
	  	if( checkRow(row) ){
	  		totalMutacionesHorizontales +=1;
	  	}
	});
	getDNAVertical(dna).forEach(function(row) {
		if(checkRow(row)){
	  		totalMutacionesVerticales +=1;
	  	}
	});

	getArrayDiagonal(dna, true).forEach(function(row) {
		if(checkRow(row)){
	  		totalMutacionesOblicuasDer +=1;
	  	}
	});

	getArrayDiagonal(dna, false).forEach(function(row) {
		if(checkRow(row)){
	  		totalMutacionesOblicuasIzq +=1;
	  	}
	});

	tieneMutacion = totalMutacionesHorizontales + totalMutacionesVerticales + totalMutacionesOblicuasDer +totalMutacionesOblicuasIzq > 0;


	return tieneMutacion;
}

/**
* @method
* @desc verifica si la fila dada tiene mutacion
* @version 1.0.0
* @param {string} row
* @returns {boolean} tieneMutacion
*/
function checkRow(row){
	let tieneMutacion = false;
	row.split("").forEach(function(letra) {
	  tieneMutacion = tieneMutacion || new RegExp(letra.repeat(4)).test(row.replace(/ /g, ""))
	});
	return tieneMutacion;
}

/**
* @method
* @desc obtiene dna en formato vertical
* @version 1.0.0
* @param {array} dna original
* @returns {array} dna vertical
*/
function getDNAVertical(dna){
	let dnaVertical = [];
	dna[0].split("").forEach(function(letra, index) {
		let newStrVertical = getletraByIndex(index, dna);
	  	dnaVertical.push(newStrVertical);
	});
	return dnaVertical;
}

/**
* @method
* @desc otiene valores en vertical 
* @version 1.0.0
* @param {number} indice del la letra
* @param {array} dna original
* @returns {string} row vertical
*/
function getletraByIndex(index, dna){
	return dna.map( (row) => {  
			return row[index]; 
		} 
	).join("");
}


/**
* @method
* @desc otiene array dna en diagonal
* @version 1.0.0
* @param {array} dna original
* @param {boolean} derecha  true diagonal derecha  false diagonal izq
* @returns {array} valores diagonales
*/
function getArrayDiagonal(dna, derecha){
	let arrayDiagonal = [];
	dna.forEach(function(row,indiceRow) {
		let dnaDiagonalDerecha = [];
	    row.split("").forEach(function(letra, indexLetra) {
	    	let str = getDiagonalDerecha(indiceRow, indexLetra, dna, derecha);
	    	if(str.length >= 4){
	    		dnaDiagonalDerecha.push(str);
	    	}
	    });
	    arrayDiagonal = arrayDiagonal.concat(dnaDiagonalDerecha)
	    	
	});
	return arrayDiagonal; 
}

function getDiagonalDerecha(indiceRow,indiceLetra,dna, der){
	let diagonalStr = "";
	dna.forEach(function(row,indice) {
		if(indice >= indiceRow && row[indiceLetra]){
			diagonalStr += row[indiceLetra];
			if(der){
				indiceLetra += 1;
			} else {
				indiceLetra -= 1;
			}
			
		}
	})
	return diagonalStr;

}


function stats(req, res){
    var resources = {
          "_id": "$valido", 
        "count" : {
            "$sum" : 1
        }
	};

    Mutation.aggregate([{
            $group: resources
        }]).exec(  (err, data) => { 
        	console.log(err)
			res.status(200).send( {
		       count_mutations:data.filter(elem => elem._id == false)[0].count,
		       count_no_mutation:data.filter(elem => elem._id == true)[0].count,
		       ratio:  data.filter(elem => elem._id == false)[0].count / data.filter(elem => elem._id == true)[0].count
		  	});
		});
}

module.exports = {
	check, stats, hasMutation
};