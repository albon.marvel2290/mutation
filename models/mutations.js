'use strict';

var moment = require('moment');
var mongoose = require('mongoose');
var moment = require('moment-timezone');
var mongoosePaginate = require('mongoose-paginate');
var Schema = mongoose.Schema;


var MutationSchema = new mongoose.Schema({
	dna: { type : [String], required : true },
	valido: { type: Boolean, required: true},
	fecha: { type: Date, default: moment().tz("America/Mexico_City").format(), require:true },
});

MutationSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Mutation' , MutationSchema);