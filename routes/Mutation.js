'use strict'

var express = require('express');
var MutationController = require('../controllers/MutationController');

var api = express.Router();

api.post('/mutation', MutationController.check);

api.get('/stats', MutationController.stats);


module.exports = api;