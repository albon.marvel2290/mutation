# Mutation

DetectA si una persona tiene diferencias genéticas basándose en su secuencia de ADN

## Getting started
Install:  npm install 

Run:  npm start 


## TEST 
    npm start 
    y en otra terminal
    npm run test

##

ADD DNA
POST
SERVER
https://3789-1eea4d3c-9e57-43c9-97da-c018786e0025.cs-us-central1-pits.cloudshell.dev/api/mutation
LOCALHOST
http://localhost:3789/api/mutation

{
    "dna":["ATGCGA","CAGTGC","TTATTT","AGACGG","GCGTCA","TTACTG"]
}

SEE STATS
GET
Server
https://3789-1eea4d3c-9e57-43c9-97da-c018786e0025.cs-us-central1-pits.cloudshell.dev/api/stats
localhost
http://localhost:3789/api/stats