'use strict';

var express = require('express');
var cors = require('cors')
var bodyParser = require('body-parser');

var app = express();
app.use(cors());
//Rutas
var _routes = require('./routes/Mutation');

//middleware bodyparser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


//base rutas
app.use('/api', _routes);



module.exports = app;